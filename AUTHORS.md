iKISS Authors
-------------

Vigouroux Y and Orjuela J. conceived the original idea.

Orjuela J. wrote iKISS package, documentation and performed the source code.

* iKISS uses kmerGWAS which was written by Voichek Y. and Weigel D. 2019.

* iKISS uses pcadapt R package which was written by Luu K. et al. 2017 and Privé F. et al 2020.

* iKISS uses also lfmm which was written by Caye K, Jumentier B and Francois O. 2021.

* iKISS uses SnakEcdysis python package created and maintained by S. Ravel to perform installation and execution in local and cluster mode.

### Others contributions

Vi Tram has fixed some important bugs in code of *pcadapt.R* script and test iKISS in coffea data.

Charriat F., Triay C., Comte A., Tranchant C., Ravel S. contributed in gitlab-ci test implementation through "kissenguele" dojo code project.

Sabot F. and Orjuela J. contributed in data test creation.

###  Contact:

Julie Orjuela: julie.orjuela@ird.fr

Yves Vigouroux: yves.vigouroux@ird.fr

## Links:

| tool | link |
|---|---|
|KmersGWAS | https://github.com/voichek/kmersGWAS |
|pcadapt | https://cran.r-project.org/web/packages/pcadapt/readme/README.html|
|lfmm | https://bcm-uga.github.io/lfmm/articles/lfmm |
|Snakecdysis: | https://snakecdysis.readthedocs.io/en/latest/package.html |
